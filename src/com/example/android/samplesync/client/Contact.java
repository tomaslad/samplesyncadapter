package com.example.android.samplesync.client;

import org.json.JSONObject;

import android.util.Log;

public class Contact {

	// Android Id
	private int contactId;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private boolean changed;
	private boolean deleted;

	public Contact(int contactId, String firstName, String lastName, String phone, String email, Boolean changed, Boolean deleted) {
		this.contactId = contactId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.email = email;
		this.changed = changed;
		this.deleted = deleted;
	}

	public int getContactId() {
		return contactId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}

	public boolean isChanged() {
		return changed;
	}

	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * Creates and returns an instance of the user from the provided JSON data.
	 * 
	 * @param contact
	 *            The JSONObject containing user data
	 * @return contact The new instance of Voiper user created from the JSON data.
	 */
	public static Contact valueOf(JSONObject contact) {
		try {
			final int contactId = contact.getInt("contactId");
			final String firstName = contact.has("firstName") ? contact.getString("firstName") : null;
			final String lastName = contact.has("lastName") ? contact.getString("lastName") : null;
			final String phone = contact.has("phone") ? contact.getString("phone") : null;
			final String email = contact.has("email") ? contact.getString("email") : null;
			final boolean changed = contact.has("changed") ? contact.getBoolean("changed") : false;
			final boolean deleted = contact.has("deleted") ? contact.getBoolean("deleted") : false;

			return new Contact(contactId, firstName, lastName, phone, email, changed, deleted);
		} catch (final Exception ex) {
			Log.i("Contact", "Error parsing JSON user object" + ex.toString());

		}
		return null;
	}
}
